<?php

namespace App\Http\Controllers;

use App\Transfer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transfers = Transfer::paginate(8);
        return view('transfers.transfers', ['transfers'=> $transfers, 'current' =>'transferred']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
     * get all fransfers
     *
     *@param int $page
     *@param string $column
     *@param string $sort {desc, asc}
     */
    public function transfers(Request $request){
        if($request->input('sort') && $request->input('column')){
            $column = $request->input('column');
            $sort = $request->input('sort');
        }else {
            $column = 'userName';
            $sort = 'ASC';
        }
        $transfers = DB::table('transfer_logs')
            ->leftJoin('users', 'transfer_logs.user_id', '=', 'users.id')
            ->leftJoin('companies', 'users.company_id', '=', 'companies.id')
            ->select('users.name as userName',  'companies.name as companyName',
                DB::raw('SUM(transferred) as total')
            )
            ->groupBy('users.name', 'companies.name')
            ->orderBy($column, $sort)
            ->paginate(8);
        return view('transfers.allTransfers', ['transfers'=> $transfers->appends(Input::except('page')), 'current' =>'transfers']);
    }
}
