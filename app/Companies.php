<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    protected $table = 'companies';
    /**
     * Get the users for the company.
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
