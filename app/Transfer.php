<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $table = 'transfer_logs';

    /**
     * Get the user for transfer.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     *
     * convert normal types format size
     *@params bigint $size
     */
    public static function convertTransferSizeByType($size){
        $types = ['KB'=>pow(2, 10), 'MB'=>pow(2, 20), 'GB'=>pow(2, 30), 'TB'=>pow(2, 40)];
        $typeArray = array_reverse($types);
        foreach ($typeArray as $k =>$v){
            if($size/$v > 1 ){
                $correctType = $k;
                $size = floor($size/$v );
                break;
            }
        }
        if(!$correctType){
            $correctType = 'KB';
        }
        return $size.$correctType;
    }

}
