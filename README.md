### Installation
1. clone this project
2. php composer install
3. create test_task db
4. configuration env
5. php artisan migrate
6. php artisan db:seed if eny errors php artisan migrate:refresh --seed
7. php artisan serve
8. run project url localhost:8000/ if eny errors php artisan serve --host=some.other.domain --port=8001