@extends('layouts.app', ['curernt'=>"transfers"])
<div class="col-md-10 col-md-offset-1">
<div id="page-wrap">
    <table class="table table-striped tbl">
        <thead>
        <tr class="bg-primary">
            <th><a href="{{ route('all_trans', ['column'=>'userName', 'sort'=> (request()->input('sort')=='desc' && request()->input('column')=='userName')? 'asc':'desc']) }}" style="color: white">
                    <span class="glyphicon glyphicon-triangle-{{(request()->input('sort')=='desc' && request()->input('column')=='userName')? 'top':'bottom'}} sg" aria-hidden="true"> </span>User Name</a></th>
            <th class="sort"> <a href="{{ route('all_trans', ['column'=>'companyName', 'sort'=> (request()->input('sort')=='desc' && request()->input('column')=='companyName')? 'asc':'desc']) }}" style="color: white">
                    <span class="glyphicon glyphicon-triangle-{{(request()->input('sort')=='desc' && request()->input('column')=='companyName')? 'top':'bottom'}}  sg" aria-hidden="true"> </span>company</a></th>
            <th><a href="{{  route('all_trans', ['column'=>'total', 'sort'=>(request()->input('sort')=='desc'  && request()->input('column')=='total')? 'asc':'desc']) }}" style="color: white">
                    <span class="glyphicon glyphicon-triangle-{{(request()->input('sort')=='desc' && request()->input('column')=='total')? 'top':'bottom'}}  sg" aria-hidden="true"> </span>Total Transfers</a></th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($transfers as $transfer)
            <tr>
                <td>{{ $transfer->userName }}</td>
                <td>{{ $transfer->companyName }}</td>
                <td>{{ \App\Transfer::convertTransferSizeByType($transfer->total) }}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-default" aria-label="Left Align">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                    </button>
                    <button type="button" class="btn btn-sm btn-default" aria-label="Left Align">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>delete
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $transfers->links() }}
</div>
    </div>