    @extends('layouts.app', ['curernt'=>"users"])
    <div class="col-md-10 col-md-offset-1">
    <div id="page-wrap">
        <table class="table table-striped tbl">
            <thead>
            <tr class="bg-primary">
                <th><span class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>Name</th>
                <th><span class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>E-mail</th>
                <th><span class="glyphicon glyphicon-triangle-bottom sg" aria-hidden="true"> </span>Company</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{App\Companies::find($user->company_id)->name }}</td>
                    <td>
                        <a href="{{ URL::to('user/edit/' . $user->id) }}"class="btn btn-sm btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                        </a>
                        <a href="{{ URL::to('user/delete/' . $user->id) }}" class="btn btn-sm btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>delete
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
    </div>