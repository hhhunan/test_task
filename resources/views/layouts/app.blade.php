<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <!-- Styles -->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
               <li class="{{$current==''?'active':''}}"><a href="{{ url('/') }}">Home <span class="sr-only"></span></a></li>
                <li class="{{$current=='users'?'active':''}}"><a href="{{ url('/users') }}">Users <span class="sr-only"></span></a></li>
                <li class="{{$current=='companies'?'active':''}}"><a href={{ url('/companies') }}>Companies <span class="sr-only"></span></a></li>
                <li class="{{$current=='transferred'?'active':''}}"><a href="{{ url('/transferred') }}">Transferred Logs <span class="sr-only"></span></a></li>
                <li class="{{$current=='transfers'?'active':''}}"><a href="/transfers">Transfers <span class="sr-only"></span></a></li>
                </ul>
            </div>
    </div>
</nav>
<div class="container">

    @yield('content')
</div>
<!-- jQuery -->
<script src="js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>