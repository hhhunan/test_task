@extends('layouts.app',['curernt'=>"companies"])
<div class="col-md-10 col-md-offset-1">
<div id="page-wrap">
    <table class="table table-striped tbl">
        <thead>
        <tr class="bg-primary">
            <th>Name</th>
            <th>Quota</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($companies as $company)
        <tr>
            <td>{{ $company->name }}</td>
            <td>{{ $company->quota }}</td>
            <td>
                <button type="button" class="btn btn-sm btn-default" aria-label="Left Align">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                </button>
                <button type="button" class="btn btn-sm btn-default" aria-label="Left Align">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>delete
                </button>
            </td>
        </tr>
            @endforeach
        </tbody>
    </table>
    {{ $companies->links() }}
</div>
</div>